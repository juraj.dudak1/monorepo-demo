/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Button,
  Alert
} from 'react-native';
import Stuffer from './Stuffer';

const App: () => React.ReactNode = () => {
  const addStuff = () => {
    Stuffer.addStuff(Stuffer.generateRandomStuff())
  }
  const getStuff = async () => {
    const stuffs = await Stuffer.getAllStuff()
    Alert.alert("Got stuff", JSON.stringify(stuffs, null, 2))
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Button title="Add stuff" onPress={addStuff}/>
        <Button title="Get all stuff" onPress={getStuff}/>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
});

export default App;
