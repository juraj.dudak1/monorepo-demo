interface Stuff {
  description: string;
  price: number;
  isGood: boolean
}

const Stuffer = {
  host: "http://10.0.2.2:1337",
  generateRandomStuff(): Stuff {
    return {
      description: `Stuff #${Math.floor(Math.random() * 1000000)}`,
      price: Math.floor((Math.random() * 1000)),
      isGood: Math.random() > 0.5
    }
  },
  async addStuff(stuff: Stuff) {
    await fetch(`${this.host}/stuffs`, {method: "POST", body: JSON.stringify(stuff)})
  },

  async getAllStuff(): Promise<Stuff[]> {
    const response = await fetch(`${this.host}/stuffs`)
    return await response.json()
  }
}

export default Stuffer