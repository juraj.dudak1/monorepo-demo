import Stuffer from '../Stuffer'
import 'isomorphic-fetch'

it("should get empty list of stuff", async () => {
  Stuffer.host = "http://localhost:1337"
  console.log(await Stuffer.getAllStuff())
})